// #ifdef MP-WEIXIN
const menuButtonInfo = uni.getMenuButtonBoundingClientRect();
const systemInfo = uni.getSystemInfoSync();
const navBarHeight = (menuButtonInfo.top - systemInfo.statusBarHeight) * 2 + menuButtonInfo.height + systemInfo.statusBarHeight;
const menuRight = systemInfo.screenWidth - menuButtonInfo.right;
const menuBotton = menuButtonInfo.top - systemInfo.statusBarHeight;
const menuHeight = menuButtonInfo.height;
const menuTop = menuButtonInfo.top;
wx.setStorageSync('navbarHeight', navBarHeight);

export {
	navBarHeight,
	menuTop
}
// #endif